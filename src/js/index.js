// JavaScript Document

// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});



(function($) {
/* ------------------------------------------------------------
 * [ Google Maps ]
 * ------------------------------------------------------------ */
	function googleMaps() {
		var center = new google.maps.LatLng(34.735909,136.511818);
		var point1 = new google.maps.LatLng(34.736644,136.512258);
		var point2 = new google.maps.LatLng(34.734586,136.511841);
		var styles = [
			{
				stylers: [
					{ hue: "#00ff33" },
					{ lightness: 0 },
					{ saturation:-70 }
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
					{ lightness: 0 }
				]
			}
		];
		var mapOptions = {
			center: center,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: styles,
			scrollwheel: false
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var mapCenter;
		google.maps.event.addListener(map, 'idle', function() {
			mapCenter = map.getCenter();
		});
		var timer = false;
		$(window).resize(function() {
			if (timer !== false) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				map.panTo(mapCenter);
			}, 0);
		});

		var offset = new google.maps.Size(0, 0);

		var icon1 = new google.maps.MarkerImage(
			'/common/images/icn_pin02.png',
			new google.maps.Size(25, 44),
			new google.maps.Point(0, 0),
			new google.maps.Point(20, 55)
		);
		var infowindow1 = new google.maps.InfoWindow({
			content: '<div class="map_infowindow"><p>Miel Citron<br>〒514-0004 三重県津市栄町4-75-1</p></div>',
			pixelOffset: offset
		});
		var marker1 = new google.maps.Marker({
			position: point1,
			map: map,
			icon: icon1,
			title: ''
		});
		google.maps.event.addListener(marker1, 'click', function() {
			infowindow1.open(map,marker1);
		});

	}
}